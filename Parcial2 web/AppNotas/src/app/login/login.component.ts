import { Component, OnInit, Output } from '@angular/core';
import { LibretaService, IMessage } from '../services/libreta.service';
import { Observable } from 'rxjs/Observable';
// INSTEAD of
import 'rxjs/Observable';
import { Router } from '@angular/router';
import { EventEmitter } from 'events';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  
 
  emailSesion: string;
  passwordSesion : string;
  algo: any;
  nombre : string = "algo";;

  @Output() emisor = new EventEmitter();

  constructor(private LibretaService: LibretaService, private router: Router) { }

  ngOnInit() {
  }

  emit(){
    this.emisor.emit(this.nombre);
  }
  
  
  initSesion(){
    let algo = {emailSesion:this.emailSesion,passwordSesion:this.passwordSesion}
    console.log(algo);
    this.LibretaService.sesion(algo).subscribe(res => {
      console.log("esto recibio "+res._body);
      if(res._body!=0){
        // this.router.navigate['/MisNotas'];
        console.log(res._body);
        this.nombre=res._body;
        localStorage.setItem("idUser", JSON.stringify(this.nombre)); 
        
        this.router.navigateByUrl("/MisNotas");
      }
      console.log('AppComponent Success', res);
    }, error => {
      console.log('AppComponent Error', error);
    })
  }

}

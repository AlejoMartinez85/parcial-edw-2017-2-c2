import { Component, OnInit } from '@angular/core';
import { LibretaService, IMessage } from '../services/libreta.service';
import { Observable } from 'rxjs/Observable';
// INSTEAD of
import 'rxjs/Observable';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  img : any ='../assets/user.png' ;
  message: IMessage = {};

  constructor(private LibretaService: LibretaService) { }

  ngOnInit() {
  }
  convierte(){
    let file: any = document.querySelector("#photo");
    file = file.files[0];  

    let blob = this.getFileBlob(file);
    blob.then(
      blob =>this.img=blob
    ).catch(function(message){
      console.log("error "+message);
    });
   
   }

  /*
  *
  * Método para convertir una URL de un archivo en un
  * blob
  * @author: pabhoz
  *
  */
  getFileBlob(file) {
    
    var reader = new FileReader();
    return new Promise(function(resolve, reject){

      reader.onload = (function(theFile) {
        return function(e) {
              resolve(e.target.result);
        };
      })(file);

      reader.readAsDataURL(file);

    });
     
  }
  sendEmail(message: IMessage) {
    this.convierte();
    this.LibretaService.sendEmail(message).subscribe(res => {
      //console.log('AppComponent Success', res);
    }, error => {
      //console.log('AppComponent Error', error);
    })
  }

}

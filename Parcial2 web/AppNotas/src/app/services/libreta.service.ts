import { Injectable } from '@angular/core';
import { Libreta } from '../models/libreta';
import { Nota } from '../models/nota';
import { Observable } from 'rxjs/Observable';
import { Resolve } from '@angular/router';
import 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {Http, Headers} from '@angular/http';
//import 'rxjs';

export interface IMessage {
  name?: string,
  email?: string,
  message?: string,
  photo?: any
}

@Injectable()
export class LibretaService {

  libretas:Libreta[]=[];
  notas:Nota[]=[];
  private emailUrl = 'http://localhost/assets/email.php';
  private sesionUrl = 'http://localhost/assets/initSesion.php';
  private notaInsertUrl = 'http://localhost/assets/insertNota.php';
  private libretaInsertUrl = 'http://localhost/assets/insertLibreta.php';
  private mostrarLibretatUrl = 'http://localhost/assets/mostrarLibreta.php';
  private eliminarLibretatUrl = 'http://localhost/assets/eliminaLibreta.php';



  constructor(private http: Http) { 

    // let n1 = new Nota("Nota1","Cuerpo1","24/10/2017","Libreta1");
    // let n2 = new Nota("Nota2","Cuerp2","24/10/2017","Libreta2");
    // let n3 = new Nota("Nota3","Cuerpo3","24/10/2017","Libreta1");
    
    // let l1= new Libreta("Libreta1",[]);
    // let l2= new Libreta("Libreta2",[]);

    // this.notas.push(n1,n2,n3);
    // this.libretas.push(l1,l2);

    for (var index = 0; index < this.notas.length; index++) {
      this.libretas.forEach((element) => {
        console.log(this.notas[index]);
        if (this.notas[index].id== element.titulo){
          element.addNotas(this.notas[index]);
        }
      });
      
    }
    

    


  }

  getLibretas(){

    // console.log(this.libretas);
    return this.libretas
    
  }

  getNotas(){
    return this.notas
  }

  sendEmail(message: IMessage): Observable<IMessage> | any {
    return this.http.post(this.emailUrl,  JSON.stringify(message))
      .map(response => {
        //console.log('Sending email was successfull', response);
        return response;
      })
      .catch(error => {
        //console.log('Sending email got error', error);
        return Observable.throw(error)
      })
  }

  mostrarLibreta(message: string): Observable<IMessage> | any {
    return this.http.post(this.mostrarLibretatUrl, {idUser:message})
      .map(response => {
        //console.log('Sending email was successfull', response);
        //let l1= new Libreta(response[1],[]);
        let putito = JSON.parse(response.text());
        console.log(putito);
        
        for (var index = 0; index < putito.length; index++) {
          var element = putito[index];
          let l1= new Libreta(element,[]);
          this.libretas.push(l1);
        }
        return response;
      })
      .catch(error => {
        //console.log('Sending email got error', error);
        return Observable.throw(error)
      })
  }
  eliminaLibreta2(body: string): Observable<IMessage> | any {
    return this.http.post(this.eliminarLibretatUrl,  JSON.stringify({title:body}))
      .map(response => {
        console.log('Sending email was successfull', response);
        return response;
      })
      .catch(error => {
        console.log('Sending email got error', error);
        return Observable.throw(error)
      })

  }
  sesion(body: any): Observable<IMessage> | any {
    return this.http.post(this.sesionUrl,  JSON.stringify(body))
      .map(response => {
        console.log('Sending email was successfull', response);
        return response;
      })
      .catch(error => {
        console.log('Sending email got error', error);
        return Observable.throw(error)
      })

  }
  insertLibreta(body: string,user: string): Observable<IMessage> | any {

    return this.http.post(this.libretaInsertUrl,  JSON.stringify({titulo:body,idUser:user}))
      .map(response => {
        console.log('Sending email was successfull', response);
        return response;
      })
      .catch(error => {
        console.log('Sending email got error', error);
        return Observable.throw(error)
      })

  }
  insertNota(body: any): Observable<IMessage> | any {
    return this.http.post(this.libretaInsertUrl,  JSON.stringify(body))
      .map(response => {
        console.log('Sending email was successfull', response);
        return response;
      })
      .catch(error => {
        console.log('Sending email got error', error);
        return Observable.throw(error)
      })

  }

}

import { TestBed, inject } from '@angular/core/testing';

import { LibretaService } from './libreta.service';

describe('LibretaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LibretaService]
    });
  });

  it('should be created', inject([LibretaService], (service: LibretaService) => {
    expect(service).toBeTruthy();
  }));
});

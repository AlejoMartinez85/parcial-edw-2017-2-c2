import { Component, OnInit, Input } from '@angular/core';
import { Libreta } from '../models/libreta';
import { LibretaService } from '../services/libreta.service';
import { Nota } from '../models/nota';

@Component({
  selector: 'app-lateral',
  templateUrl: './lateral.component.html',
  styleUrls: ['./lateral.component.scss']
})
export class LateralComponent implements OnInit {

  libretas:Libreta[];
  notas:Nota[];
  texto : string;
  crearLibreta:string="";
  crearNota:string="";
  algo: any;
  user: string;

  constructor(private ls: LibretaService) { }
  
  ngOnInit() {
    
    this.libretas = this.ls.getLibretas();
    this.notas= this.ls.getNotas();
    // console.log(this.notas);
    this.user = JSON.parse(localStorage.getItem("idUser"));
    this.texto=this.user;
    console.log("esto fue lo que guardo el local "+this.user);

    this.ls.mostrarLibreta(this.user).subscribe(res => {
      console.log("esto recibio "+res._body);
       
    }, error => {
      console.log('AppComponent Error', error);
    })
    
    // console.log(this.texto);
  }
  
  // agregarNota(){
  //   this.notas.push(new Nota(this.crearNota,"hola","hola","hola"));

  // } 
  
  addLibreta(){
    this.libretas.push(new Libreta(this.crearLibreta,));

    this.ls.insertLibreta(this.crearLibreta,this.user).subscribe(res => {
      console.log("esto recibio "+res._body);
       
    }, error => {
      console.log('AppComponent Error', error);
    })
  }


  eliminarLibreta(algo){
    for (var index = 0; index < this.libretas.length; index++) {
      if (this.libretas[index].titulo== algo) {
            this.libretas.splice(index);
              
      }
            
    } 

    this.ls.eliminaLibreta2(algo).subscribe(res => {
      console.log("esto recibio "+res._body);
       
    }, error => {
      console.log('AppComponent Error', error);
    })
    
  
    
   }
   
  //  addNota(){ 
  //   for(var index = 0; index < this.notas.length; index++) {
  //     this.libretas.forEach((element) => {
  //       // console.log(this.notas[index]);
  //       if (this.notas[index].id== element.titulo){

  //         element.addNotas(this.notas[index]);

  //       }
  //     });
      
  //   }
  //  }
   
  eliminarNota(notaTitulo){
  //  for (var index = 0; index <this.libretas.length; index++) {
  //   this.notas.forEach((element) => {
  //     console.log(this.notas[index].titulo);
  //     if (this.notas[index].titulo= notaTitulo){
  //       this.notas.splice(index);

  //       this.libretas[index].notas[index].titulo

  //     }
  //   });
     
  //  }
  for (var index = 0; index < this.libretas.length; index++) {
    
    for (var i = 0; i < this.notas.length; i++) {
  
      if (this.notas[i].titulo == notaTitulo) {

      // this.libretas.splice(this.notas[i].titulo);  
        
      }
      
    }
    
  }
  }


}

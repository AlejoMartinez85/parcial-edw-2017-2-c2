import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotaEscrituraComponent } from './nota-escritura.component';

describe('NotaEscrituraComponent', () => {
  let component: NotaEscrituraComponent;
  let fixture: ComponentFixture<NotaEscrituraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotaEscrituraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotaEscrituraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

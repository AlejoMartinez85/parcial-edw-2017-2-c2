import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { Observable }     from 'rxjs/Observable'; 

import {HttpModule} from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LateralComponent } from './lateral/lateral.component';
import { UnionComponent } from './union/unioncomponent';
import { NotFoundComponent } from './not-found/not-found.component';
import { NotaEscrituraComponent } from './nota-escritura/nota-escritura.component';
import { LibretaService } from './services/libreta.service';

const appRoutes: Routes = [
  { path: 'Login', component: LoginComponent },
  { path: 'Register', component: RegisterComponent },
  { path: 'MisNotas', component: UnionComponent },
  {
    path: '',
    redirectTo: '/Login',
    pathMatch: 'full'
  },
  {
    path: 'Register',
    redirectTo: '/Register',
    pathMatch: 'full'
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    LateralComponent,
    UnionComponent,
    NotFoundComponent,
    NotaEscrituraComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    HttpModule

  ],
  providers: [LibretaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
